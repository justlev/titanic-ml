from speedml import Speedml
sml = Speedml('./train.csv', './test.csv', target = 'Survived', uid = 'PassengerId')
sml.shape()
sml.configure('overfit_threshold', sml.np.sqrt(sml.train.shape[0]) / sml.train.shape[0])
